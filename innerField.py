from __future__ import division			# to get division with rest from python 3
from vertex import Vertex


class InnerField:

	def __init__(self, boundaryList):
		print "InnerField initialized."
		self.vertices = []
		self.findCentrePoint(boundaryList)

	def findCentrePoint(self, boundaryList):
		xSum = 0
		ySum = 0
		noNodes = 0

		for bd in boundaryList:
			for vt in bd.vertices:
				xSum = xSum + vt.crd[0]
				ySum = ySum + vt.crd[1]
				noNodes = noNodes + 1

		xMean = xSum/noNodes
		yMean = ySum/noNodes
		self.vertices.append( Vertex( [xMean, yMean] ) ) 
		print xMean, yMean