from meshIO import MeshIO
from boundary import Boundary
from innerField import InnerField

import matplotlib.pyplot as plt


class Mesh:

	def __init__(self):
		self.ioObj = MeshIO()

		self.boundaryList = []
		for c in self.ioObj.readGeometry():
			self.boundaryList.append(  Boundary( c )	)

		# inner Field is constructed with the boundary nodes
		self.innerField = InnerField( self.boundaryList )		


		print "mesh initialized."

	def plot(self):

		fig = plt.figure()
		axH = fig.gca()		# axis handle

		self.ioObj.geometryPlot( axH, self.boundaryList )
		self.ioObj.gridPlot( axH, self.boundaryList, self.innerField  )
		plt.axis('equal')
		plt.show()

		self.ioObj.printSth()

