from __future__ import division			# to get division with rest from python 3

from vertex import Vertex

from sympy import *
from sympy.geometry import *

class Boundary:

	def __init__(self, inCurve ):
		self.curve = inCurve
		self.vertices = []
		noIntersec = 16
		eval_ = 0	#	curve parameter
		while ( eval_ < 1 ):
			vrt = self.curve.subs( self.curve.parameter ,eval_ )		#Point2D type
			self.vertices.append( Vertex(  [ vrt[0], vrt[1] ]  ) )
			# self.vertices.append( self.curve.subs( self.curve.parameter ,eval_ ) )
			eval_ = eval_ + 1/noIntersec

		# print "Boundary initialized."