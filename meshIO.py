import math

# import networkx as nx
import matplotlib.pyplot as plt

from sympy import *
from sympy.geometry import *
from sympy.abc import t
from sympy.plotting import plot_parametric
import numpy as np


class MeshIO:

	def __init__(self):
		self.someVariable
		self.readGeometry()


	def readGeometry(self):
		geometry = Circle
		curves = []
		if (geometry == Triangle ):
			# p1 = Point(0, 0)
			# p2 = Point(1, 0)
			# p3 = Point(1, 1)
			# curves.append( Curve( (t, interpolate([1, 4, 9, 16], t)), (t, 0, 1)) )
			return curves
			
		# elif( geometry == Circle ):
		# 	R = 10
		# 	curves.append( Curve( (R*sin(2*math.pi*t), R*cos(2*math.pi*t)), (t, 0, 1)) )
		# 	return curves

		elif( geometry == Circle ):
			R = 10
			curves.append( Curve( ( t*R, 0 ), (t, 0, 1) ) )
			curves.append( Curve( (R*cos(0.5*math.pi*t), R*sin(0.5*math.pi*t)), (t, 0, 1)) )
			curves.append( Curve( ( 0, R*(1-t) ), (t, 0, 1) ) )
			return curves

		else :
			print "no geometry"


	def printSth(self):
		print "moin moin"

	def gridPlot(self, axH, boundaryField, internalField ):
		arX = [] 
		arY = [] 

		for b in boundaryField:
			for vt in b.vertices:
				arX.append(	vt.crd[0] )
				arY.append(	vt.crd[1] )

		for vt in internalField.vertices:
			arX.append(	vt.crd[0] )
			arY.append(	vt.crd[1] )
			
		axH.plot( arX, arY, 'rx')


	def geometryPlot(self, axH, boundaryField):
		supportNo = 100
		arX = [] 
		arY = [] 

		for b in boundaryField:
			fX = b.curve.functions[0]
			fY = b.curve.functions[1]
			curveParam  = np.linspace(0, 1, supportNo)
			for phi in curveParam:
				arX.append( fX.subs( b.curve.parameter, phi ) )
				arY.append( fY.subs( b.curve.parameter, phi ) )
		axH.plot(arX, arY, 'g--')
		